# No 1
Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya

# No 2
Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya

# No 3
Menampilkan use case table untuk produk digitalnya

# No 4
Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya: 

# No 5
Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya: 

# No 6
Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan 
- Regex
- Substring
- Sisanya bebas

# No 7
Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya

# No 8
Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya

# No 9
Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya: 
- Minimal 1 Foreign Key
- Minimal 1 Index
- Minimal 1 Unique Key

# No 10
Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

# No 11
Bonus: Mendemonstrasikan UI untuk CRUDnya
